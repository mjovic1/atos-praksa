package hr.atos.praksa.mateojovic.zadatak06;

import java.util.Scanner;

public class Zadatak06 {
	
	public static void ispisTablice(int[][] tablica_mnozenja,  int brojac, String ime) {
		
		
		
		System.out.println("-------------------------------");
		System.out.println(": : :  TABLICA  MNOZENJA  : : :");
		System.out.println("-------------------------------");
		System.out.println(" * |  1  2  3  4  5  6  7  8  9");
		System.out.println("-------------------------------");
		
		for(int i=1; i<10; i++) {
			System.out.print(" " + brojac  + " |");
			brojac+=1;
			for(int j=1; j<10; j++) {
				if(i*j < 10) {
					System.out.print("  " + i*j);	
				}else {
					System.out.print(" " + i*j);
				}
			}
			System.out.println();
		}
		for(int i = 0; i< 31 - (ime.length()+3); i++) {
			if(i % 2 == 0) {
				System.out.print(" ");
			}else {
				System.out.print(":");
			}
		}
		System.out.print("by " + ime);
		
	}

	
	
	public static void main(String[] args) {
		
		int[][] tablica_mnozenja;
		tablica_mnozenja = new int[10][10];
		int brojac = 1;
		String ime;
		
		System.out.println("Unesite ime");
		Scanner input = new Scanner(System.in);
		ime = input.nextLine();
		
		ispisTablice(tablica_mnozenja, brojac, ime);
		
	}
}
