package hr.atos.praksa.mateojovic.zadatak08;

import java.util.Scanner;

public class Zadatak08 {
	
	public static void ispisKalendara(int redniBrojMjeseca) {
		
		int zadnjiDan;
		if(redniBrojMjeseca == 1 || redniBrojMjeseca == 3 || redniBrojMjeseca == 5 || redniBrojMjeseca == 7 ||  redniBrojMjeseca == 8 || redniBrojMjeseca == 10 || redniBrojMjeseca == 12) {
			zadnjiDan = 31;
		}else if (redniBrojMjeseca == 2){
			zadnjiDan = 29;
		}else {
			zadnjiDan = 30;
		}
		
		System.out.println(" P  U  S  �  P  S  N");
		for(int i = 1; i<=zadnjiDan; i++) {
			
			if(i == 1 || i == 8) {
				System.out.print(" " + i);
			}else if(i ==15 || i == 22 || i == 29) {
				System.out.print(i);
			}else if(i < 10) {
				System.out.print("  " + i);
			}else {
				System.out.print(" " + i);
			}
			
			if(i % 7 == 0) {
				System.out.println();
			}
			
		}
		
		
		
	}

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		int redniBrojMjeseca;
		int brojDanaMjesec;
		do {
			System.out.println("Unesi redni broj mjeseca");
			redniBrojMjeseca = input.nextInt();
			if(redniBrojMjeseca > 12 || redniBrojMjeseca < 1) {
				System.out.println("Pogresan unos, ponovite");
			}
		}while(redniBrojMjeseca > 12 || redniBrojMjeseca < 1);
		
		ispisKalendara(redniBrojMjeseca);
		
		
		
	}
}
