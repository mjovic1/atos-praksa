package hr.atos.praksa.mateojovic.zadatak09;

import java.util.Scanner;

public class Zadatak09 {
	
	public static int[] generiranjeTroskovaStruje() {
		Scanner input = new Scanner(System.in);
		
		int []trosakStruje;
		int []zaokruzenTrosak;
		trosakStruje = new int[12];
		zaokruzenTrosak = new int[12];
		
		for(int i=0; i<12; i++) {
			System.out.println("Unesi tro�ak struje za " + (i+1) + ". mjesec");
			trosakStruje[i] = input.nextInt();
		}
		for (int i=0; i<12; i++) {
			if(trosakStruje[i] % 1000 >500) {
				zaokruzenTrosak[i] = (trosakStruje[i] /1000 + 1) * 1000;
			}else {
				zaokruzenTrosak[i] = (trosakStruje[i] /1000) * 1000;
				if(zaokruzenTrosak[i] < 0) {
					zaokruzenTrosak[i] = 0;
				}
			}
		}
		
		return zaokruzenTrosak;
	}
	
	public static void ispisTroskova (int[] zaokruzenTrosak, int broj) {

		
		for (int i = 0; i< 11; i++) {
			
			if(i % 2 == 0) {
				
				if(broj > 0) {
					System.out.print(broj + "kn - |");
					for (int j=0; j<12; j++) {
						if(zaokruzenTrosak[j] == broj ) {
							System.out.print(" x ");
						}else {
							System.out.print("   ");
						}	
					}
					broj-=1000;
				}else {
					System.out.print("   " + broj + "kn - |");
					for (int j=0; j<12; j++) {
						if(zaokruzenTrosak[j] == broj ) {
							System.out.print(" x ");
						}else {
							System.out.print("   ");
						}		
						
					}
				}
				
			}else {
				System.out.println();
				System.out.println("         |");
			}
		}
		System.out.println();
		System.out.println("          __ __ __ __ __ __ __ __ __ __ __ __ ");
		System.out.println("           1  2  3  4  5  6  7  8  9 10 11 12");
		
		return;
	}

	public static void main(String[] args) {
		

		int []zaokruzenTrosak;
		zaokruzenTrosak = new int[12];
		int broj = 5000;
		
		zaokruzenTrosak = generiranjeTroskovaStruje();
		
		
		
		/*for(int i=0; i<12; i++) {
			System.out.print(zaokruzenTrosak[i] + " ");
		}
		System.out.println();
		*/System.out.println();
		
		ispisTroskova(zaokruzenTrosak, broj);
		
		
		
	}
}
