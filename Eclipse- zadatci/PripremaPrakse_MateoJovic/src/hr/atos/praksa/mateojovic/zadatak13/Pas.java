package hr.atos.praksa.mateojovic.zadatak13;

public class Pas implements Zivotinja {

	public void kreceSe() {
		System.out.println("seta na cetri noge");
	}
	
	public void glasaSe(String glasanje) {
		System.out.println(glasanje);
	}
	
	
	
	public static void main(String[] args) {
	
		Pas bobi = new Pas();
		bobi.kreceSe();
		bobi.glasaSe("vau");
	}
}
