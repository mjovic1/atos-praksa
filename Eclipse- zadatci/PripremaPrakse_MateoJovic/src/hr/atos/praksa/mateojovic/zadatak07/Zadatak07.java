package hr.atos.praksa.mateojovic.zadatak07;

import java.util.Scanner;

public class Zadatak07 {
	private static void dijeljiviS6(int manji, int veci) {
		int brojac = 0;
		
		for(int i = (manji + 1); i<veci; i++) {
			if(i % 6 == 0) {
				brojac++;
			}
		}
		
		System.out.println("U intervalu izme�u <" + manji + "," + veci + "> ima " + brojac + " broja koji su djeljivi s 6");
	}
	public static void main(String[] args) {
		
		int a, b;
		Scanner input = new Scanner(System.in);
		
		System.out.println("Unesi jedan prirodan broj");
		a = input.nextInt();
		System.out.println("Unesi drugi prirodan broj");
		b = input.nextInt();
		
		if(a>b) {
			dijeljiviS6(b, a);
		}else {
			dijeljiviS6(a, b);
		}
	}
}
