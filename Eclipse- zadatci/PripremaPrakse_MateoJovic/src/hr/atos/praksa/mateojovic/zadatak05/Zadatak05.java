package hr.atos.praksa.mateojovic.zadatak05;

import java.util.Scanner;


public class Zadatak05 {
	
	private static int odabirPocetka() {
		Scanner input = new Scanner(System.in);
		int broj1;
		
		do {
			System.out.println("Unesi broj u intervatu <0,10>");
			broj1 = input.nextInt();
		}while(broj1 >=10 || broj1 <= 0);
		
		return broj1;
	}
	
	private static int odabirKraja() {
		Scanner input = new Scanner(System.in);
		int broj1;
		
		do {
			System.out.println("Unesi broj isključivo veci od 100");
			broj1 = input.nextInt();
		}while(broj1 <= 100);
		
		return broj1;
	}
	
	private static int[] generiranjePolja(int[] polje, int broj_pocetak,int razlika) {
		
		int a = broj_pocetak;

		for(int i = 0; i < razlika; i++) {
			polje[i] = a;
			a++;
		}
		
		
		return polje;
	}
	
	private static void ispisivanjeBrojaca(int[] polje, int brojac, int razlika) {
		
		for(int i = 0; i < razlika; i++) {
			if(polje[i] >=75 && polje[i] % 20 != 0) {
				System.out.println(brojac);
			}
			if(polje[i] % 20 == 0) {
				
			}else {
				if(polje[i] <= 18) {
					brojac+=4;
				}
				if(polje[i] >= 18) {
					brojac-=1;
				}
				
			}
		}
		
		
		return;
	}
	

	
	public static void main(String[] args) {
		
		
		int broj_pocetak;
		int broj_kraj;
		int brojac = 0;
		
		broj_pocetak = odabirPocetka();
		broj_kraj = odabirKraja();
		
		int razlika = broj_kraj - broj_pocetak;
		int polje[];
		polje = new int[razlika];
		
		polje = generiranjePolja(polje, broj_pocetak, razlika);
		
		ispisivanjeBrojaca(polje,brojac,razlika);
		
		
		
	}

}
