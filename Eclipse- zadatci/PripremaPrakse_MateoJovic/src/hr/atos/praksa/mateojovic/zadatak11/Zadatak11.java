package hr.atos.praksa.mateojovic.zadatak11;

import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Scanner;

public class Zadatak11 {

	
	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		int jezik = 0;
		String language;
		Locale lokalni;
		ResourceBundle tekst;
		
		
		System.out.println("Izaberi jezik: \n1)Engleski \n2)Spanjolski \n3)Grcki");
		do {
			jezik = input.nextInt();
			if(jezik < 1 || jezik > 3) {
				System.out.println("Pogresan unos, unesite ponovno");
			}
			
		}while(jezik < 1 || jezik > 3);
		
		if(jezik == 1 ) {
			language = new String("en");
		}else if(jezik == 2) {
			language = new String("es");
		}else {
			language = new String("el");
		}
		

		
		lokalni = new Locale(language);
		 tekst = ResourceBundle.getBundle("hr.atos.praksa.mateojovic.zadatak11.Config", lokalni);
		
		System.out.println(lokalni.toString());
		
		System.out.println(tekst.getString("pozdrav"));
		System.out.println(tekst.getString("opce_pitanje"));
		System.out.println(tekst.getString("opce_pitanje_1"));
		
		
		
		
		
		
		
	}
}
