package hr.atos.praksa.mateojovic.zadatak10;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.Scanner;

public class Zadatak10 {

	public static void main(String[] args) throws IOException {
		
		String putanjaDirektorij, fraza;
		String[] lista;
		int brojac = 0;
		int brojacFraza = 0;
		Scanner input = new Scanner(System.in);
		
		System.out.println("Unesite putanju za zeljeni direktorij");
		putanjaDirektorij = input.nextLine();
		
		File dir = new File(putanjaDirektorij);
		
		if(dir.isDirectory()) {
			System.out.println("Unesite frazu koju �elite pretra�iti");
			fraza = input.nextLine();
			lista = dir.list();
			
			if(lista != null) {
				
				for(int i=0; i< lista.length; i++) {
					String naziv = lista[i];
					
					if(naziv.endsWith(".txt") || naziv.endsWith(".TXT") || naziv.endsWith(".csv") || naziv.endsWith(".CSV")) {
						String putanjaFile = dir + "\\" + naziv;
						System.out.println(putanjaFile);
						brojac++;
						try {
							BufferedReader in= new BufferedReader(new FileReader(putanjaFile));
							File velicinaFile = new File(dir + "\\" + naziv);
						
							int lines = 0;
							
							try(LineNumberReader lnr = new LineNumberReader(new FileReader(velicinaFile))){
								while (lnr.readLine() != null) {
									lines = lnr.getLineNumber();
								}
								
							}catch(IOException e) {
								
							}
							
							
							for(int j = 0; i< lines; i++) {
								String linija = in.readLine();
								if(linija.toLowerCase().contains(fraza.toLowerCase())) {
									brojacFraza++;
									//System.out.println("pojavila se");
								}
								
								
							}
							
							System.out.println(fraza + " se pojavila " + brojacFraza + " puta unutar " + naziv + " file-a" );
							brojacFraza = 0;
						} catch (FileNotFoundException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							System.out.println("Sranje, ne radi");
						}
						 
					}
					
					
				}
				if(brojac == 0) {
					System.out.println("U " + putanjaDirektorij + " se ne nalazi niti jedan file s .txt ili .csv ekstenzijom");
				}
				
			}else {
				System.out.println("Unutar navedenog direktorija nema ni jedan file ili datoteka");
			}
			
			
		}else {
			System.out.println("Putanja nije to�na, zavrsetak programa");
		}
	}
}
