package hr.atos.praksa.mateojovic.zadatak12;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class Zadatak12 {
	
	public static void main(String[] args) throws FileNotFoundException {
		
		String putanja;
		Scanner input = new Scanner(System.in);
		
		
		System.out.println("Unesi putanju do file-a");
		putanja = input.nextLine();
		File dir = new File(putanja);
		Integer brojac;
		
		
		if(dir.isFile()) {
			Map<String, Integer> rijeci;
			rijeci = new TreeMap<String, Integer>();
			Scanner datoteka = new Scanner(new File(putanja));
			while(datoteka.hasNext()) {
				String rijec = datoteka.next();
				rijec = rijec.toLowerCase();
				brojac = rijeci.get(rijec);
				if(brojac != null) {
					brojac++;
				}else {
					brojac=1;
				}
				
				rijeci.put(rijec, brojac);
			}
			
			System.out.println("U datoteci XX nalaze se sljedece rijeci:");
			System.out.println("------------------------");
			System.out.println("Rijec  (broj ponavljanja)");
			System.out.println("-------------------------");
			rijeci.forEach((k,v)->System.out.printf("%s (%s)\n", k,v));
			System.out.println("-------------------------");
			
		}else {
			System.out.println("Pogre�an unos, zavr�etak programa");
		}
		
		
		
	}
}
