package hr.atos.praksa.mateojovic.zadatak14;

import java.util.Scanner;

public class Main {

	public static double[] odabir() {
		
		double polje[];
		polje = new double[4];
		Scanner input = new Scanner(System.in);
		
		System.out.println("Unesi A u A*funk(x)+B ");
		polje[0] = input.nextDouble();
		System.out.println("Unesi B u A*funk(x)+B ");
		polje[1] = input.nextDouble();
		do {
			System.out.println("Unesi to�ku T1");
			polje[2] = input.nextDouble();
			System.out.println("Unesi to�ku T2 (mora biti ve�a od T1)");
			polje[3] = input.nextDouble();
			if(polje[2] > polje[3]) {
				System.out.println("Pogresan unos, ponovite");
			}
		}while(polje[2] > polje[3]);
		System.out.println("Uneseni parametri:\nA =  " + polje[0] + "cm\nB = "  + polje[1] + "cm\nT1 = " + polje[2] + "cm\nT2 = " + polje[3] + "cm");

		
		
		
		return polje;
		
	}
	
	
	
	public static void main(String[] args) {
		
		double polje[];
		polje = new double[4];
		
		polje = odabir();
		int izbor;
		
		
		
		Funkcija funkcija = new Funkcija(polje[0], polje[1], polje[2], polje[3]);
		
		izbor = funkcija.odabir();
		funkcija.izracunFunkcije(izbor);
		
		
	}
}
