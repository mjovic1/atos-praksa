package hr.atos.praksa.mateojovic.zadatak14;

import java.util.Scanner;

public class Funkcija implements Opis{
	
	private double A;
	private double B;
	private double T1;
	private double T2;

	public Funkcija(double A, double B, double T1, double T2) {
		this.A = A;
		this.B = B;
		this.T1 = T1;
		this.T2 = T2;
		
	}
	
	
	public static int odabir() {
		
		Scanner input = new Scanner(System.in);
		int izbor;

		do {
			System.out.println("Unesite: 1-sin 2-cos 3-tan 4-kotanges");
			izbor = input.nextInt();
		}while(izbor < 1 || izbor > 4);
		
		
		
		return izbor;
	}
	
	public void izracunFunkcije(int izbor) {
		
		double rezultat;
		String funkcija;
		if(izbor == 1) {
			funkcija = "sin";
			rezultat = A*-Math.cos(T2 - T1)+B;
		}else if(izbor == 2) {
			funkcija = "cos";
			rezultat =  A*-Math.sin(T2 - T1)+B;
		}else if(izbor == 3) {
			funkcija="tan";
			rezultat =  (A*Math.sin(T2)+B) / (A*Math.cos(T1) + B);
		}else  {
			funkcija="ctg";
			rezultat =  (A*Math.cos(T2)+B) / (A*Math.sin(T1) + B);
		}
		
		System.out.println("Povr�ina funkcija A*"+ funkcija + "(x)+B je " + rezultat + " cm2");
		
		
	}
	
	
	
	
	
	
}
