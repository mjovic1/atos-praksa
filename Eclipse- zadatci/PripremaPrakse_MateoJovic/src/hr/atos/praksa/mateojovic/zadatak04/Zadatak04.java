package hr.atos.praksa.mateojovic.zadatak04;

public class Zadatak04 {

	public static void main(String[] args) {
		
		int[] polje = { 5, 23, 43, 111, 213};
		
		for (int i=0; i<5; i++) {
			if(polje[i] % 2 == 0) {
				
				System.out.println( + polje[i] +" je paran broj");
				if(polje[i] % 3 == 0) {
					System.out.println(polje[i] + " je visekratnik broja 3");
				}
				if(polje[i] % 5 == 0) {
					System.out.println(polje[i] + " je visekratnik broja 5");
				}
				if(polje[i] % 11 == 0) {
					System.out.println(polje[i] + " je visekratnik broja 11");
				}
				
				System.out.println();
			}else {
				
				System.out.println(polje[i] + " je neparan broj");
				
				if(polje[i] % 3 == 0) {
					System.out.println(polje[i] + " je visekratnik broja 3");
				}
				if(polje[i] % 5 == 0) {
					System.out.println(polje[i] + " je visekratnik broja 5");
				}
				if(polje[i] % 11 == 0) {
					System.out.println(polje[i] + " je visekratnik broja 11");
				}
				System.out.println();
			}
		
		}
	}
}
